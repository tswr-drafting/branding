Design Guidelines for TSWR
==========================

This document details design guidelines that should be followed in order to maintain a consistent aesthetic across official documents and designs of TSWR.

* **Lines** should be heavy and fairly simple.
* **Shapes** should be basic and rounded.
* **Textures** should be smooth and plain.
* **Colors** should be few, and bright with a low saturation, achieving a small palette of "pastel" colors.
* There should be a large amount of **space**, achieving a minimal composition.

